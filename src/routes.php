<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
/**
$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});
**/
$app->get('/', function (Request $request, Response $response) {
  $this->logger->info("Slim-Skeleton '/' route");
  $_response = array ();
  $_response['title'] = 'SLiMS Official API';
  $_response['version'] = '1.0';
  return $response->withJson($_response);
});

