<?php
namespace Slims\Bibliography\Controllers;
use Slims\Helpers\Validation as H_VA;

// Functions
class Biblio
{
  private $container;
  private $db;
  private $biblio_data;
  public function __construct($container)
    {
	    $this->db = $container->db;
      $this->container = $container;
    }

    public function root($request, $response, $args)
    {
      $this->biblio_data = new \Slims\Bibliography\Models\BiblioData($this->db);
      $response = $response->withHeader('Content-type', 'application/json');
      return $this->container->view->render($response, 'biblio_list.php', [
        'biblio_list' => $this->biblio_data->get_BiblioList($request, $response, $args)
      ]);
    }

    public function detail($request, $response, $args)
    {
      $val = new H_VA;
      if ($val->is_integer($args['biblio_id'])) {
        $this->biblio_data = new \Slims\Bibliography\Models\BiblioData($this->db);
        $response = $response->withHeader('Content-type', 'application/json');
        return $this->container->view->render($response, 'biblio_detail.php', [
          'biblio_detail' => $this->biblio_data->get_BiblioDetail($request, $response, $args)
        ]);
      } else {
        $_response = array ();
        $_response['status'] = 'error';
        $_response['message'] = 'Invalid biblio_id';
        return $response->withJson($_response);
      }

    }

}
