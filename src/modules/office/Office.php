<?php

namespace Modules\office;

use \PhpOffice\PhpSpreadsheet\Spreadsheet;
use \PhpOffice\PhpSpreadsheet\IOFactory;
use \PhpOffice\PhpSpreadsheet\Writer\Xlsx;

// Functions
class Office
{
    private $db;
		private $result;
    public function __construct($container)
    {
        $this->db = $container->db;
				$this->result = $container->result;
    }

    public function root($request, $response)
    {
				$this->result['e'] = 0;
				$this->result['m'] = "Hello world from spreadsheet.";
        return $response->withJson($this->result);
    }

		public function file($request, $response)
		{
			$sth = $this->db->prepare("SELECT b.title, b.isbn_issn, b.call_number
				FROM biblio b
				LIMIT 1000");
			$sth->execute();
			$biblio = $sth->fetchAll();
			$spreadsheet = new Spreadsheet();
			$spreadsheet->getActiveSheet()
				->fromArray($biblio, NULL, 'A1');
			$writer = new Xlsx($spreadsheet);
			$response = $response->withHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
				->withHeader('Content-Disposition', 'attachment; filename="spreadsheet.xlsx"')
				->withHeader('Content-Transfer-Encoding', 'binary')
				->withHeader('Pragma', 'no-cache');
			$writer->save('php://output');
			return $response;
		}
}
