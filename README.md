# SLiMS PWA - API Backend

Backend yang menyediakan REST API

## Install the Application

- Clone repository ini
  `git clone https://gitlab.com/slims-pwa/slims-back.git`
- masuk ke directory hasil clone.
  `cd slims-backend`
- Jika menggunakan virtual host, arahkan documant root ke folder `public/`
- pastikan folder `log/` bisa ditulis. _( writeable )_
- jalankan aplikasi dengan command
  `php composer.phar start`
  atau
  `php -S localhost:8080 -t public`
