<?php

// Routes

$app->get('/', 'Slims\Bibliography\Controllers\Biblio:root');
$app->get('/biblio/{biblio_id}', 'Slims\Bibliography\Controllers\Biblio:detail');
