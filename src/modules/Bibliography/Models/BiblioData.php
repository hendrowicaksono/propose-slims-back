<?php

namespace Slims\Bibliography\Models;

// Functions
class BiblioData
{
  private $db;
  public function __construct($db)
  {
    $this->db = $db;
  }

  public function get_BiblioList($request, $response, $args)
  {
    $_response = array(); $i = 0;
    $sql = 'SELECT b.* FROM biblio AS b LIMIT 0, 10';
    $stmt = $this->db->prepare($sql);
    $stmt->execute();
    while ($row = $stmt->fetch()) {
      $_response[$i]['biblio_id'] = $row['biblio_id'];
      $_response[$i]['title'] = $row['title'];
      $i++;
    }
    return $_response;
  }

  public function get_BiblioDetail($request, $response, $args)
  {
    $_response = array();
    $sql = 'SELECT b.* FROM biblio AS b WHERE biblio_id=:biblio_id';
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(':biblio_id', $args['biblio_id']);
    $stmt->execute();
    if ($stmt->rowCount()) {
      while ($row = $stmt->fetch()) {
        $_response['biblio_id'] = $row['biblio_id'];
        $_response['title'] = $row['title'];
        $_response['sor'] = $row['sor'];
        $_response['edition'] = $row['edition'];
      }
    } else {
      $_response['status'] = 'no result';
    }
    return $_response;
  }


}
