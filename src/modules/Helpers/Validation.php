<?php
namespace Slims\Helpers;

class Validation
{

  public function is_integer($number)
  {
    $v = new \Valitron\Validator(array('number' => $number));
    $v->rule('integer', 'number');
    if($v->validate()) {
      #echo "Yay! We're all good!";
      return TRUE;
    } else {
      // Errors
      #print_r($v->errors());
      return FALSE;
    }
  }

}