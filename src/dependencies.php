<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

// PDO database library
$container['db'] = function ($c) {
    $settings = $c->get('settings')['db'];
    $pdo = new PDO(sprintf('mysql:host=%s;dbname=%s', $settings['host'], $settings['dbname']), $settings['user'], $settings['pass'], $settings['options']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $pdo;
};

// Default result
$container['result'] = function ($c) {
    return [
        'e' => 1,
        'm' => 'Something error',
    ];
};

$container['Slims\Bibliography\Controllers\Biblio'] = function ($c) {
    return new \Slims\Bibliography\Controllers\Biblio($c);
};

$container['Slims\Office'] = function ($c) {
		return new \Slims\office\Office($c);
};

$container['view'] = function ($c) {
  $settings = $c->get('settings')['renderer'];
  return new \Slim\Views\PhpRenderer($settings['template_path']);
};